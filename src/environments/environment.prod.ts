export const environment = {
    production: true,
    hmr: false,
    firebase: {
        apiKey: 'AIzaSyBqlzv1zuc6CwB4T-XuHS87LHo-CjKomO4',
        authDomain: 'rescue-kick.firebaseapp.com',
        databaseURL: 'https://rescue-kick.firebaseio.com',
        projectId: 'rescue-kick',
        storageBucket: 'rescue-kick.appspot.com',
        messagingSenderId: '266754006434',
        appId: '1:266754006434:web:6b07483e6558d44d'
    }
};
