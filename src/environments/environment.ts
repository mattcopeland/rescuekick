// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyC3bbYma1FhWUS4HnOUOyAAr1F38Tr4aNs',
    authDomain: 'rescue-kick-dev.firebaseapp.com',
    databaseURL: 'https://rescue-kick-dev.firebaseio.com',
    projectId: 'rescue-kick-dev',
    storageBucket: 'rescue-kick-dev.appspot.com',
    messagingSenderId: '129685405659',
    appId: '1:129685405659:web:0719dff470509f99'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
