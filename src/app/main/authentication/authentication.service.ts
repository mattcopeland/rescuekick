import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NotificationService } from 'app/core/services/notification.service';
import { IUser } from 'app/main/user/user.model';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Injectable()
export class AuthenticationService {
  user: Observable<IUser | null>;
  currentUser: IUser | null;
  redirectUrl: string = '';
  isVerified: Observable<boolean>;

  /**
   * Constructor
   *
   * @param _afs
   * @param _afAuth
   * @param _router
   * @param _snackBar
   */
  constructor(
    private _afs: AngularFirestore,
    private _afAuth: AngularFireAuth,
    private _router: Router,
    private _notificationService: NotificationService,
    private _httpClient: HttpClient
  ) {
    // Get ht everified state of the user
    this.isVerified = this._afAuth.authState.pipe(
      switchMap(user => {
        return of(user && user.emailVerified);
      })
    );

    // Get auth data and then get firestore user document || null
    this.user = this._afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this._afs.doc<IUser>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
    this.setCurrentUser();
  }

  /**
   * Get the current user
   */
  getCurrentUser(): IUser {
    return this.currentUser;
  }

  /**
   * Login using Angular Fire Auth
   *
   * @param email
   * @param password
   */
  loginUser(email: string, password: string): void {
    this._afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(data => {
        if (!data.user.emailVerified) {
          this._router.navigate(['/auth/mail-confirm']);
        } else if (this.redirectUrl) {
          this._router.navigateByUrl(this.redirectUrl);
        } else {
          this._router.navigate(['/rfq']);
        }
      })
      .catch(error => this._notificationService.error(error.message, 3000));
  }

  /**
   * Logout using Angular Fire
   */
  logout(): void {
    this._router.navigate(['/auth/login']);
  }

  /**
   * Register using Angular Auth
   * then create a user in the DB
   *
   * @param registerData
   */
  registerUser(registerData: any): Observable<any> {
    return this._httpClient.post('api/accounts/register', registerData);
  }

  /**
   * Reset password using Angular Fire Auth
   *
   * @param actionCode
   * @param email
   * @param newPassword
   */
  resetPassword(actionCode: string, email: string, newPassword: string): void {
    this._afAuth.auth
      .confirmPasswordReset(actionCode, newPassword)
      .then(response => {
        this.loginUser(email, newPassword);
      })
      .catch(error => this._notificationService.error(error.message));
  }

  /**
   * Set the current user
   */
  private setCurrentUser(): void {
    this.user.subscribe(user => (this.currentUser = user));
  }

  /**
   * Sets user data to firestore after succesful register
   *
   * @param user
   * @param registerData
   */
  private updateUserData(user: IUser, registerData: any): Promise<void> {
    const userRef: AngularFirestoreDocument<IUser> = this._afs.doc(
      `users/${user.uid}`
    );

    const data: IUser = {
      uid: user.uid,
      email: user.email,
      firstName: registerData.firstName,
      lastName: registerData.lastName,
      photoURL: 'assets/images/profile-photos/profile.jpg',
      roles: {
        subscriber: true
      }
    };

    return userRef.set(data, { merge: true });
  }
}
