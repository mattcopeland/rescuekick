export interface IUser {
  uid: string;
  companyId?: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;
  firstName?: string;
  lastName?: string;
  roles?: IRole;
  phone?: number;
  jobTitle?: string;
}

export interface IRole {
  subscriber?: boolean;
  admin?: boolean;
}

export interface IUserAddress {
  address1: string;
  address2?: string;
  city: string;
  state: string;
  zip: string;
}
