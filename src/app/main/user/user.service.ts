import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NotificationService } from 'app/core/services/notification.service';
import { IUser, IUserAddress } from 'app/main/user/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  usersCollection: AngularFirestoreCollection<IUser>;
  users: Observable<IUser[]>;

  userAddressesCollection: AngularFirestoreCollection<IUserAddress>;
  userAddresses: Observable<IUserAddress[]>;

  /**
   * Constructor
   * @param afs
   * @param _notificationService
   */
  constructor(
    private _afs: AngularFirestore, private _notificationService: NotificationService) {
    this.usersCollection = this._afs.collection('users');
    this.users = this.usersCollection.valueChanges();
  }

  /**
   * Get full list of all users
   *
   * @returns Observable<User[]>
   */
  getUsers(): Observable<IUser[]> {
    return this.users;
  }

  /**
   * Gets the addresses for a user
   *
   * @param uid user id
   * @returns Observable<UserAddress[]>
   */
  getUserAddresses(uid: string): Observable<IUserAddress[]> {
    this.userAddressesCollection = this._afs.collection('userAddresses', ref =>
      ref.where('uid', '==', uid).orderBy('edited')
    );
    this.userAddresses = this.userAddressesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as IUserAddress;
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );

    return this.userAddresses;
  }

  /**
   * Updates the user's profile data
   *
   * @param uid user id
   * @param data updated profile data
   * @returns Promise<any>
   */
  updateContactInfo(uid: string, data: any): Promise<any> {
    return this._afs.collection('users').doc(uid).update(data)
      .then(() => this._notificationService.success('Contact Info Updated'))
      .catch(function (error: any): any {
        console.error('Error updating info: ', error);
      });
  }
}
