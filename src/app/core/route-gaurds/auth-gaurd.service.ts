import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

import { AuthenticationService } from '../../main/authentication/authentication.service';

@Injectable()
export class AuthGaurd implements CanActivate {
  constructor(private _router: Router, private _authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    return this._authenticationService.user.pipe(
      take(1),
      map(user => !!user),
      tap(loggedIn => {
        if (!loggedIn) {
          this._authenticationService.redirectUrl = state.url;
          this._router.navigate(['/auth/login']);
        }
      })
    );
  }
}

@Injectable()
export class VerifiedGaurd implements CanActivate {
  constructor(private _router: Router, private _authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    return this._authenticationService.isVerified.pipe(
      take(1),
      map(verified => !!verified),
      tap(verified => {
        if (!verified) {
          this._authenticationService.redirectUrl = state.url;
          this._router.navigate(['/auth/mail-confirm']);
        }
      })
    );
  }
}
