import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(public _snackBar: MatSnackBar) { }

  /**
   * Display notification
   *
   * @param message
   * @param panelClass
   * @param duration
   */
  notify(message: string, panelClass?: string, duration?: number): MatSnackBarRef<SimpleSnackBar> {
    this.snackBarRef = this._snackBar.open(message, null, {
      duration: duration || 5000,
      panelClass: panelClass
    });
    return this.snackBarRef;
  }

  /**
   * Display success notification
   *
   * @param message
   * @param duration
   */
  success(message: string, duration?: number): MatSnackBarRef<SimpleSnackBar> {
    this.snackBarRef = this._snackBar.open(message, null, {
      duration: duration || 5000,
      panelClass: 'green-600'
    });
    return this.snackBarRef;
  }

  /**
   * Display error notification
   *
   * @param message
   * @param duration
   */
  error(message: string, duration?: number): MatSnackBarRef<SimpleSnackBar> {
    this.snackBarRef = this._snackBar.open(message, null, {
      duration: duration || 5000,
      panelClass: 'warn'
    });
    return this.snackBarRef;
  }
}
