import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpCacheService {
  requests: any = {};

  constructor() { }

  /**
   * Puts the http response into the cache
   *
   * @param url
   * @param response
   */
  put(url: string, response: HttpResponse<any>): void {
    this.requests[url] = response;
  }

  /**
   * Gets the http reponse from cache
   *
   * @param url
   */
  get(url: string): HttpResponse<any> | undefined {
    return this.requests[url];
  }

  /**
   * Invalidates the cache for a spedified http response
   *
   * @param url
   */
  invalidateUrl(url: string): void {
    this.requests[url] = undefined;
  }

  /**
   * Invalidates all the http cache
   */
  invalidateCache(): void {
    this.requests = {};
  }
}
