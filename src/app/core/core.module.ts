import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material';

import { AuthenticationService } from '../main/authentication/authentication.service';
import { HttpCacheInterceptor } from './interceptors/http-cache-intreceptor.service';
import { AuthGaurd, VerifiedGaurd } from './route-gaurds/auth-gaurd.service';
import { HttpCacheService } from './services/http-cache.service';

@NgModule({
  imports: [MatSnackBarModule],
  providers: [
    AuthenticationService,
    AuthGaurd,
    VerifiedGaurd,
    HttpCacheService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpCacheInterceptor,
      multi: true
    }
  ],
  exports: []
})
export class CoreModule { }
